# Bowling for Brains

#### Laatste Update

Goed, precies vandaag en dus ook echt midden in een commit/push sluit Ziggo me af.
Er moest echt een usb stick aan te pas komen en een mobiele horspot, maar de files zoals bedoeld staan hier nu iig goed.


#### What, How & Where


* Implementatie van BC Dev assignment: 10 pin bowling (traditional) scoring system. 
  
* Minimale, maar wel goed werkende, web-app  -live-  te zien en te testen: 

  http://exactamundo.nl/bowling/index.html

* Git: Public Fork van originele project:

  https://gitlab.com/bowling-bc/bowling-for-brains

* Als ik nog tijd heb, maak ik nog een Node.js server in de cloud met custom Docker install. 
  Die komt dan hier: 

* Als niet, dan staat hier de code:  **lit-element-starter-js-master/dev**
  



#### Brainstorms
----------------

* After reading the bowling rules for 3 times, I needed to go read the rules on Wikipedia; sorry, I never bowled :) The "score is awarded" part is a bit cryptic, if you ask me, but okay - the least of my problems.

* To be complety honest, after half an hour of coding sth in Python (okay maybe an hour), I started to wonder about 'the big picture', and I returned to the assignment description... This is an old habit of mine, that just doesnt want to die it seems: I dive in a bit too enthousiastically and ussualy end up doing sth not so pragmatical: Back to the lab.

Requirements
We purposely choose something relatively simple and open so that you can show us how you reason and develop a complete application.
The minimum requirements:

1. Correctly calculates bowling scores

2. Add a variable number of players with names

3. Manually add scores for each player (so we can test)

4. Tracks the number of games won for each player

5. Reset all game stats and start over

6. Visual representation using your component framework of choice (bonus points for usingwebcomponents (lit-element/haunted etc.)


* Ok, deliberately simple and -open- gotya. After reading the whole assigment three times this time (..) I came up with, I think, a proper choice of tools and a working plan:  

* TOOLSET: It seems that everything can be done on the frontside, so Javascript it is. Also no need for database / or AJAX then. As far as the visual representation goes I haven't really looked at things like React.js yet, so I choose to learn sth about webcomponents. 

* WORKING PLAN: First create git repo & checkout etc. Then create a single html file, and a js file and then start to code the actual 'bowling magic'. I'll try to keep OOP and design patterns etc. in mind, but since this isnt' exactly big data, first I just want sth that works, and test it in the console. When the scoring calculator works, add players and inputs etc. as requested in the assigment, and then continue with the webcompoments part. Only after this maybe add error-handling, testing, improving quality of code and/or add more interesting features. If there's time, perhaps add a back-end component, like Node.js, so that it's a complete app. Don't forget to commit once in a while ;)

* Chose gitlab as the place to be for the repo and followed best practices: 

  https://www.datree.io/resources/github-best-practices

  Here, I didn't do a lot more than creating a potected master, a develop branche and looked at the .gitignored file after some time.

* Got some working calculating JS. i'm thinking now that the 'player' part is probably meant to show /suited to throw in at least some OOP, so let's.

* Read about webcomponents for half a day... perhaps not the best way of spending my time, but at least it seems very doable now. Haunted looks too new, I can't find a lot of references for this, and lit-elements seems pretty good, is or at least was developed by Google, and I can find a lot more info on this tool. Haunted also has a few drawbacks, but is seems when I want to use lit-elemnt the right way node.js is the recommended way, and I was planning on making this project just a little more extended than a html and a js file... so.. let's go!

* I will create a node.js dev server at least somewhere hosted on a cloud and checkout my repo there, so it's also directly accessable.

* Found an interesting set of wecomponents: wiredjs.

https://www.webcomponents.org/element/wired-combo/classes/WiredCombo

https://github.com/wiredjs/wired-elements

* Instead of making my own in lit-element, I decided to go for these nice-looking ones... sigh... thought it would save me some time... Little did he know: Lost half a day looking for my own shadow in the shadow DOM. The combination of dynamically created elements and when they are rendered, not being able to query the shadow DOM directly (first not even knowing what it was) and the fact that JQuery doesn't work with the shadow DOM (or at least not directly, inuitively), well.. caused me some headaches. In the end, I prevailed! :) And let's put it like this: I learned a lot :)




#### ToDo
---------


* remove eventlisteners when buttons enabled
* restrictions on valid Bowling scores inputs, like: when strike, throw2 disables, 
  or when 7, only 1/2/spare as options
* restrictions in the code with error messages
* what tot do when equal?
* setting names should work different
* scores should/could be resetted when new game 
* js/jquery inconsistancies
* making sth similar, but with building my own, simpler webcomponent, 
  which was probably what the idea was in the first place...
* testing, hosting, cloud, docker, etc etc etc




#### Afsluiting
---------------


Maar ja.. je moet -ergens- stoppen ;)

Dit is een goeie opdracht imho. 

Doordat het heel open is, word je gedwongen (mag je) om zelf keuzes te maken en zo loop je (liep ik) tegen dezelfde dingen aan als in de praktijk. In mijn geval is dat denk ik: soms ergens te snel induiken en soms iets te lang blijven hangen bij een eerder gemaakte keuze en misschien ook te snel iets te groot (willen) maken. Kwaliteit, kwantiteit enzo. Maar ala, ik krijg het iig steeds sneller door - haha

Verder kun je doordat het open is misschien laten zien waar je interesses of vaardigheden liggen, door te kiezen waar je aandacht aan besteedt - ookal kwam dat bij mij niet helemaal uit de verf, vind ik zelf, omdat ik juist over iets stuikelde waar ik nog nooit mee gewerkt had.

Nouja, dat is sws een ding, want die keuze was vooral gebaseerd op dat ik ook nog niet met dingen als Angular of React of Vue.js heb gewerkt en dat is misschien ook een goed antwoord op de eerder gestelde vraag wat ik wil leren (naast andere, zoals Machine Learning e.d. ambities) : ik ben er een behoorlijk tijdje tussenuit geweest (en in ICT land gaat de tijd nog sneller dan normaal) en ik wil weer met een beetje vertrouwen kunnen developpen, op z'n minst een beetje te weten wat nu 'oldskool' is en wat niet :) Zo kwam ik een nieuwe editor tegen (VSCode) waar ik best blij mee ben.

Ach, zolang stackoverflow nog maar bestaat eigenlijk- haha

Ok, verder mondeling..



Grts,
Lance


