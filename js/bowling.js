/* 
 *  Bowling.js:  Create a small OOP bowling world that we can use in our mini app:
 *
 * 	Bowling for brains
 */

// Used 'factory' design pattern as opposed to class/constructor.
// There are pro's and cons for both, but hey, it's javascript and this is a mini-app:)

const Game = function() {
	const game = {
		players : [],
		names : [],
		winner : -1,
		// Just create the player objects, add scores later in order to be able to play a new game with the same players.
		createPlayer : (name, id) => {
			let player = Player(name, id);
			game.names.push(name);
			game.players.push(player);
			return;
		},

		addScoresPlayer : (id, ballThrows) => {
			game.players[id].throws = ballThrows;
		},

		play : () => {
			for (var player of game.players) {
				player.calcScore();
			} 
			// What if equal scores?? :) Can't find it in 10 mins googling..
			// Finding the winner could be done a lot better, but it works for now.
			var maxScore = Math.max.apply(Math, game.players.map( function(player) { return player.score; }));
			var winners = game.players.filter(function(player){ return player.score == maxScore; });
			if (winners.length == 1) {
				game.winner = winners[0];
				game.winner.gamesWon++;
				game.printWinner(1);
			}
			else {
				game.printWinner(0);
			}
			for (var p of game.players) {
				p.printScore();
			}
		},

		printWinner : (doWeHaveAWinner) => {
			if (doWeHaveAWinner) {
				document.getElementById("errorMessage").innerHTML = game.winner.playerName + 
				"    ...is the winner with a score of no less than:     " + game.winner.score + " !"; 
			} 
			else {
				document.getElementById("errorMessage").innerHTML = "Unbelievable... It's a Tie!  (now what?! ;)";
			}
		},

		newGame : () => {
			game.winner = -1;
			for (var player of game.players) {
				player.score = 0;
				player.throws = [];
			}  
			var scores = document.getElementsByClassName("score");
			for (let i = 0; i < scores.length; i++) {
				scores[i].innerHTML = "";
			}
			document.getElementById("errorMessage").innerHTML = "&nbsp;"; 
		},
		// Start over with the current players.
		newRound : () => { 
			game.winner = -1;
			for (var player of game.players) {
				player.score = 0;
				player.throws = [];
				player.gamesWon = 0;
			}  
			game.winner = -1;
			var scores = document.getElementsByClassName("score");
			for (let i = 0; i < scores.length; i++) {
				scores[i].innerHTML = "";
			}
			var totals = document.getElementsByClassName("total");
			for (let j = 0; j < totals.length; j++) {
				totals[j].innerHTML = "";
			}
			document.getElementById("errorMessage").innerHTML = "&nbsp;"; 
		},

		reset : () => {
			game.players = [];
			game.names = [];
			game.winner = -1;
		},
		// Complete reset all game data and start over from scratch.
		// First I wanted to delete the game object, but apparently that's not possible in JS :)
		resetAll : () => {
			game.reset();
			// Couldnt get this thing completely cleared otherwise...
			var pInput = document.getElementById('playerInput');
			var clone = pInput.cloneNode(true);
			pInput.remove();
			document.getElementById('place2be').appendChild(clone);
			document.getElementById('playerInput').disabled = false;
        	document.getElementById('fill').disabled = false;
			document.getElementById('startGame').disabled = false;
			document.getElementById('setGame').disabled = true;
			document.getElementById('calculateScores').disabled = true;
			document.getElementById('newGame').disabled = true;
			document.getElementById('newRound').disabled = true;
			document.getElementById('reset').disabled = true;
			document.getElementById("errorMessage").innerHTML = "&nbsp;"; 
			// Zooo...
			document.getElementById("players").innerHTML = "";
		},
	};
	return game;
}

const Player = function(name, id, ballThrows = []) {
	const player = {
		ID : id,
		playerName : name,
		throws : ballThrows,
		score : 0,
		gamesWon : 0,
		// A helper function tot calculate the score. The score for a throw depends on the previous throw. 
		throwScore : (ballThrow, previousThrow) =>  {
			if (!isNaN(ballThrow)) {
				return parseInt(ballThrow);
			}
			// Another approach would be to use the 'values' of the select input and let X and / be 10 and then
			// a strike can be only thrown in the first throw etc. but then the 10th frame would be more complex.
			else if (ballThrow === 'x' || ballThrow === 'X') {
					return 10;
				}
				else if (ballThrow === '/') {
					// Complete score to 10, including the score of the pevious ball.
					// If previous ball was '-', just add 10.
					return (isNaN(previousThrow) ? 10 : 10 - previousThrow);
				}
				else { 
					// ballTrow === '-' scores 0 points.
					return 0;
				}
		},
		// Loop through the ballthrows
		calcScore : () =>  {
			var tempScore = 0;
			for (var i = 0; i < player.throws.length; i++) {
				tempScore += player.throwScore(player.throws[i], player.throws[i - 1]);
				// Strike! Add the next two ballThrows as well.
				if (player.throws[i] === 'x' || player.throws[i] === 'X' ) {
					tempScore += player.throwScore(player.throws[i + 1]);
					tempScore += player.throwScore(player.throws[i + 2], player.throws[i + 1]);
					// If this was the first ball of the 10th frame: stop.
					if (i == length - 3) {
						break;
					}
				}
				// Spare! Add the next ballThrow as well.
				else if (player.throws[i] === '/') {
					tempScore += player.throwScore(player.throws[i + 1], player.throws[i]);
					//  If this was the second ball of the 10th frame: stop.
					if (i == length - 2) {
						break;
					}
				}
			}
			player.score = tempScore;
		},
	
		printScore : () => {	
			document.getElementById('player_' + player.ID + '_score').innerHTML = player.score;   
			document.getElementById('player_' + player.ID + '_total').innerHTML = player.gamesWon;  
		},
	};
	return player;
}

/* 
 * Some  dirty debugging shizzle.
 *
 * calcScore(["8", "-", "7", "-", "5", "3", "9", "/", "9", "/", "X", "8", "-", "5", "1", "3", "/", "9", "-"]); 
 * calcScore(["8", "/", "9", "/", "4", "4", "7", "2", "9", "-", "X", "X", "8", "-", "3", "5", "9", "/", "7"]); 
 *
 * console.log("Throws:");		
 * console.log(ballThrows);
 */
